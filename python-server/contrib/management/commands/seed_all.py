from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, **options):
        print('no seeds defined so far')
