from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, **options):
        call_command('flush', interactive=False)
        call_command('initialize')  # Making sure there as the admin user used for API calls
