from django.core.management import call_command
from django.core.management.base import BaseCommand

from contrib.helpers import create_dev_superuser


class Command(BaseCommand):
    def handle(self, **options):
        create_dev_superuser()
        call_command('seed_all')
