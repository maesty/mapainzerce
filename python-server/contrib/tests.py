from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase

from contrib.helpers import create_dev_superuser, SUPERUSER_EMAIL, SUPERUSER_PASSWORD

API_BASE = settings.API_BASE


class SuperUserTestCase(TestCase):
    def setUp(self):
        self.superuser = create_dev_superuser()

    def test_user_is_superuser(self):
        self.assertTrue(self.superuser.is_superuser)
        self.assertTrue(self.superuser.is_staff)
        self.assertTrue(self.superuser.is_active)

    def test_we_know_his_credentials(self):
        user = authenticate(username=SUPERUSER_EMAIL, password=SUPERUSER_PASSWORD)
        self.assertIsInstance(user, User)
        self.assertEqual(self.superuser.id, user.id)
