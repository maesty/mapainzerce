from django.contrib.auth.models import User


SUPERUSER_EMAIL = 'admin@admin.com'
SUPERUSER_PASSWORD = 'admin'


def create_dev_superuser():
    superuser = User.objects.filter(username=SUPERUSER_EMAIL).first()
    if not superuser:
        superuser = User.objects.create_superuser(SUPERUSER_EMAIL, SUPERUSER_EMAIL, SUPERUSER_PASSWORD)
    return superuser
