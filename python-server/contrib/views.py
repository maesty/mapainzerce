from django.conf import settings
from django.core.management import call_command
from django.http import Http404, HttpResponse
from django.views import View


class CommandView(View):
    def get(self, request, command):
        if settings.COMMAND_VIEW is not True:
            raise Http404()

        call_command(command)
        return HttpResponse('OK')
