import os

from django.conf import settings
from django.core.files import File
from django.core.management.base import BaseCommand

from geo.models import MapObject


class Command(BaseCommand):
    def handle(self, **options):
        folder_path = os.path.join(settings.BASE_DIR, '..', 'region_data')
        for filename in os.listdir(folder_path):
            django_file = File(open(os.path.join(folder_path, filename), 'r'))

            map_obj, _ = MapObject.objects.get_or_create(name=filename)
            map_obj.source_file = django_file
            map_obj.save()
