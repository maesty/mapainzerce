from django.contrib import admin

from geo.models import Region, MapObject


class RegionAdmin(admin.StackedInline):
    model = Region


class MapObjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'regions',)
    inlines = (RegionAdmin,)

    def regions(self, map_obj: MapObject):
        return ', '.join(map_obj.regions.all().values_list('label', flat=True))

    regions.short_description = 'regiony'


admin.site.register(MapObject, MapObjectAdmin)
