# Generated by Django 2.2 on 2020-04-22 13:38

from django.db import migrations


def clean_cached_content(apps, schema_editor):
    MapCachedContent = apps.get_model('geo', 'MapCachedContent')
    for cached_content in MapCachedContent.objects.all():
        cached_content.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0011_auto_20200422_1310'),
    ]

    operations = [
        migrations.RunPython(clean_cached_content),
    ]
