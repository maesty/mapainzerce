# Generated by Django 2.2 on 2020-04-22 10:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0005_auto_20200422_1023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='map',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='regions', to='geo.MapObject'),
        ),
    ]
