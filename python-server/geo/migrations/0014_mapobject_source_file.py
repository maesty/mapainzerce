# Generated by Django 2.2 on 2020-05-06 15:16

from django.db import migrations, models
import geo.models


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0013_auto_20200422_1958'),
    ]

    operations = [
        migrations.AddField(
            model_name='mapobject',
            name='source_file',
            field=models.FileField(blank=True, null=True, upload_to='', validators=[geo.models.validate_file_extension], verbose_name='nastavit dle souboru (.csv)'),
        ),
    ]
