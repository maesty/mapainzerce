import re

import requests
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models


MAPY_CZ_PATTERN = r'mapy\.cz/\w+\?x=(?P<lat>\d{1,2}\.\d+)&y=(?P<lng>\d{1,2}\.\d+)&z=(?P<zoom>\d{1,2})$'


class Sector(models.Model):
    slug = models.SlugField(unique=True)
    sector_name = models.CharField(max_length=75)
    group_name = models.CharField(max_length=75)
    volume = models.IntegerField()

    class Meta:
        ordering = ('slug',)


class Point(models.Model):
    sector = models.ForeignKey(Sector, on_delete=models.CASCADE, related_name='points')
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    class Meta:
        ordering = ('id',)


def validate_file_extension(value):
    if not value.name.endswith('.csv'):
        raise ValidationError('Pouze .csv soubory jsou přijímány')


class MapObject(models.Model):
    name = models.CharField(max_length=75, verbose_name='název mapy')
    source_file = models.FileField(null=True, blank=True, verbose_name='nastavit dle souboru (.csv)',
                                   validators=[validate_file_extension])

    initial_map_view = models.URLField(null=True, blank=True, verbose_name='nastavení výchozího pohledu mapy',
                                       help_text='URL z mapy.cz', validators=[RegexValidator(MAPY_CZ_PATTERN)])

    class Meta:
        ordering = ('id',)
        verbose_name = 'mapa'
        verbose_name_plural = 'mapy'

    def __str__(self):
        return f'mapa: {self.name}'

    def save(self, *args, **kwargs):
        self.full_clean()

        super().save(*args, **kwargs)

        if hasattr(self, 'cached_content'):
            self.cached_content.delete()

        for region in self.regions.all():
            region.save()

        if self.source_file:
            self.load_form_file()
            self.source_file = None
            self.save()

        self.refresh_from_db()
        return self

    def clean(self):
        if self.source_file:
            content = self.source_file.file.read()
            if type(content) is bytes:
                try:
                    content = content.decode('utf8')
                except UnicodeDecodeError:
                    content = content.decode('cp1250')

            lines = content.splitlines()
            self.validate_data_format(lines[1:])

    def load_form_file(self):
        with self.source_file.open(mode='rb') as f:
            content = f.read()
            try:
                content = content.decode('utf8')
            except UnicodeDecodeError:
                content = content.decode('cp1250')

            lines = content.splitlines()[1:]

        # data lines are valid, let's do clean import
        self.regions.all().delete()

        regions = {}

        # initiate session (obtain all needed cookies)
        session = requests.session()
        session.get('https://www.cedi.cz/enter.do')
        session.get('https://www.cedi.cz/client/show_distribution_order_map.do',
                    params={
                        'modelId': 'distribution_order1',
                        'id': '-1'})

        for line in lines:
            sector_slug, sector_name, sector_volume, region_label = line.split(';')

            if region_label not in regions:
                regions[region_label] = Region.objects.create(map=self, label=region_label)

            region = regions[region_label]
            sector = Sector.objects.filter(slug=sector_slug).first()
            print('sector', sector_slug)
            if not sector:
                print('fetching...')
                sector = Sector(slug=sector_slug, group_name=sector_name, volume=sector_volume)

                response = session.post('https://www.cedi.cz/maps/get_segment_map.sdo', data={'code': sector_slug})
                result = response.json()

                sector.sector_name = result['polygon'][0]['name']
                sector.save()

                points = []
                for point in result['polygon'][0]['points']:
                    points.append(Point(
                        sector=sector,
                        latitude=point[0],
                        longitude=point[1]))

                Point.objects.bulk_create(points)

            region.sectors.add(sector)

    @staticmethod
    def validate_data_format(lines):
        for line in lines:
            if not re.match(r'^\d+;[^;]+;\d+;.+', line):
                raise ValidationError('Data nejsou v požadovaném formátu '
                                      '(číslo sektoru; název sektoru; množství vydání; popisek sektoru')


class Region(models.Model):
    map = models.ForeignKey(MapObject, on_delete=models.CASCADE, related_name='regions')

    label = models.CharField(max_length=140, verbose_name='popisek')
    color = models.CharField(max_length=6, verbose_name='barva regionu', default='0000ff',
                             help_text='HEX kód barvy bez #, např.: "9060ff" nebo "000"',
                             validators=[RegexValidator('^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')])

    sectors = models.ManyToManyField(Sector, blank=True, editable=False)
    sectors_text = models.TextField(default='', blank=True,
                                    verbose_name='seznam sektorů',
                                    help_text='na každý řádek kód jednoho sektoru')

    def __str__(self):
        return f'region: {self.label} - {self.map}'

    def save(self, *args, **kwargs):
        """
        On every save convert `sectors_text` to sectors relationship.
        """
        super().save(*args, **kwargs)

        if self.sectors_text:
            sector_slugs = self.sectors_text.split('\n')
            sectors = []

            for sector_slug in sector_slugs:
                sector_slug = sector_slug.strip()
                if sector_slug:
                    sector = Sector.objects.filter(slug=sector_slug).first()
                    if not sector:
                        raise ValueError(f'Sector "{sector_slug}" does not exists!')

                    sectors.append(sector)

            self.sectors.set(sectors)

        self.sectors_text = '\n'.join(self.sectors.all().values_list('slug', flat=True))
        super().save()

        self.refresh_from_db()
        return self


class MapCachedContent(models.Model):
    map = models.OneToOneField(MapObject, on_delete=models.CASCADE, related_name='cached_content')
    content = models.TextField()
