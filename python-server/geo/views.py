import json
import re
from collections import OrderedDict

from django.shortcuts import get_object_or_404, render

from geo.models import MapObject, MapCachedContent, MAPY_CZ_PATTERN


def map_list(request):
    map_objects = MapObject.objects.all()
    return render(request, 'map_list.html', locals())


def map_view(request, map_id):
    map_object = get_object_or_404(MapObject, pk=map_id)

    try:
        regions_json = map_object.cached_content.content
        regions = json.loads(regions_json)

    except MapCachedContent.DoesNotExist:
        regions = []
        for region in map_object.regions.all():
            regions.append({
                'name': region.label,
                'sectors': [],
                'sector_groups': [],
            })

            # group sectors by name
            sector_groups = {}
            for sector in region.sectors.all():
                sector_groups.setdefault(sector.group_name, 0)
                sector_groups[sector.group_name] += sector.volume

            sector_groups = [
                {
                    'name': name,
                    'volume': volume,
                }
                for name, volume in sector_groups.items()
            ]

            sector_groups.sort(key=lambda s: -s['volume'])
            regions[-1]['sector_groups'] = sector_groups

            for sector in region.sectors.all():
                points = OrderedDict()
                for point in sector.points.all():
                    points[f'[{point.latitude},{point.longitude}]'] = True

                points = [
                    json.loads(k)
                    for k, v in points.items()
                ]

                regions[-1]['sectors'].append({
                    'name': sector.group_name,
                    'points': points,
                    'volume': sector.volume,
                    'color': region.color,
                })

        regions_json = json.dumps(regions)

        map_object.cached_content = MapCachedContent.objects.create(
            map=map_object,
            content=regions_json
        )

    if map_object.initial_map_view:
        initial_view = re.search(MAPY_CZ_PATTERN, map_object.initial_map_view).groupdict()

    return render(request, 'map_view.html', locals())
