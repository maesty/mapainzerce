from django.urls import path

from geo.views import map_view, map_list

urlpatterns = [
    path('', map_list, name='map_list'),
    path('map/<int:map_id>/', map_view, name='map_view'),
]
