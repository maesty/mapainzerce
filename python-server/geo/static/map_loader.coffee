

for region in regions
  for sector in region.sectors
    for point, i in sector.points
      sector.points[i] = SMap.Coords.fromWGS84(point...)

if latitude and longitude and zoom
  center = SMap.Coords.fromWGS84(latitude, longitude)
  map_zoom = zoom

else
  center   = sector.points[0]
  map_zoom = 13

m = new SMap(JAK.gel("m"), center, map_zoom)
m.addDefaultLayer(SMap.DEF_BASE).enable()
m.addDefaultControls()

layer = new SMap.Layer.Geometry()
m.addLayer(layer)
layer.enable()


for region in regions
  for sector in region.sectors
    options =
      color: '#' + sector.color
      opacity: 0.5
      outlineWidth: 0

    layer.addGeometry(new SMap.Geometry(SMap.GEOMETRY_POLYGON, null, sector.points, options))


# fixes https://app.clickup.com/t/4urz53
window.onresize = -> window.location.reload()
