.PHONY: build run dev no_python test clean

build:
	echo '.git' > .dockerignore
	cat .gitignore >> .dockerignore
	docker-compose -f docker-compose.yaml build

run:
	 docker-compose -f docker-compose.yaml up -d

dev:
	trap 'make clean' EXIT; docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up

no_python:
	trap 'make clean' EXIT; docker-compose -f docker-compose.no_python.yaml up

test:
	rm -Rf ./*data_test
	trap 'make clean' EXIT; docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run backend test

clean:
	docker-compose -f docker-compose.yaml down --remove-orphans
